//bank elements
const elBankBalance = document.getElementById('bank-balance')
const elLoanBalance = document.getElementById('loan-balance')
const elLoanField = document.getElementById('loan-field')
const elLoanButton = document.getElementById('get-loan')

//work elements
const elWorkBalance = document.getElementById('work-balance')
const elBankTransferButton = document.getElementById('work_transfer-bank')
const elWorkButton = document.getElementById('add-work')
const elPayLoanButton = document.getElementById('pay-loan')

//computer elements
const elComputersSelectBar = document.getElementById('computers')
const elFeatureHeader = document.getElementById('feature-header')
const elComputerFeature = document.getElementById('computer-feature')

//infosection elements
const elInfoSection = document.getElementById('info-section')
const elComputerImage = document.getElementById('computer-image')
const elComputerName = document.getElementById('computer-name')
const elSummaryInfo = document.getElementById('summary-info')
const elPrice = document.getElementById('price')
const elBuyComputerButton = document.getElementById('buy-computer')

//return elements
const elReturnText = document.getElementById('return-text')


//creating computers and keeping them in a list
const COMPUTERS = [
    new Computer(1, 'Macbook Air', 1300, 'This computer is light as Air, perfect for the commuters', ['Rose Gold', 'Sleek', 'Perfectly rounded edges'], 'img/MacBookAir.jpg'),
    new Computer(2, 'Macbook Pro', 2000, 'This computer is superior', ['Silver', 'Powerful', 'The greatest performance you will experience'],'img/MacBookPro.jpg' ),
    new Computer(3, 'iMac', 1600, 'This is an all in one computer for your home office', ['Silver', 'Huuuge screen', 'Powerful tools'],'img/iMac.jpg' ),
    new Computer(4, 'iMac Pro', 6000, 'This computer is for the professionals', ['Carbon silver', 'The best colour graphics the human eye will ever see', 'Turbo boost'],'img/iMacPro.jpg' )
]

//constructor for computers
function Computer(id, name, price, description, feature, image) {
    this.id = id
    this.name = name
    this.price = price
    this.description = description
    this.feature = feature
    this.image = image
}


//adding computers as options in the selector
for (const computer of COMPUTERS){
    const elComputerOption = document.createElement('option')
    elComputerOption.value = computer.id
    elComputerOption.innerText = computer.name
    elComputersSelectBar.appendChild( elComputerOption )
}


//initialise values i need to keep track of
let bankBalance = 0
let loanAmount = 0
let earned = 0
let computerPrice = 0
let computerBought = false


//setting the text of bankBalance and workBalance to the start value
elBankBalance.innerText = bankBalance +' kr.'
elWorkBalance.innerText= earned + 'kr.'


//button-listener if one wants a loan
elLoanButton.addEventListener('click', function (){
    //create a popup box to get the loan amount form user
    let elLoanAmount = Number(prompt("Enter the amount you want to loan:", ""))
    while (elLoanAmount != null){ //while loop to keep prompting user for correct input
        if(! isNaN(elLoanAmount)){ //check if input is a number
           if  (elLoanAmount > 0 && elLoanAmount/2 <= bankBalance){ //check if you can get a loan
               loanAmount = elLoanAmount //setting loan amount
               bankBalance += loanAmount; //adding loan to bankBalance
               elLoanField.innerText='Loan: ' //adding text to the loan field so that the user can see their loan
               elLoanBalance.innerText = loanAmount + ' kr.' //adding the loanAmount shown to user
               elBankBalance.innerText = bankBalance + ' kr.' //updating the bankBalance shown to user
               elLoanAmount = null; //ending the while loop
               elLoanButton.disabled=true //disabling the loan button so it cannot be clicked until the user satisfies the loan constraints
               elPayLoanButton.style.display="initial" //showing the payLoan button to user
           }
           else{ //cannot get loan because of balance
               alert("You need to earn more money before you can get a loan")
               elLoanAmount = null; //ending the while loop
           }
        }
        else{ //if not a number ask for amount again
        elLoanAmount = Number(prompt("Not a number! Enter the amount you want to loan:", ""))
        }
    }
})


//button-listener if one wants to transfer earned money to the bank
elBankTransferButton.addEventListener('click', function (){
    if (loanAmount > 0){ //checking if one has a loan
        let loanTransfer = (earned * 10) /100 //calculating 10 percent of earned money to pay down the loan
        if (loanTransfer > loanAmount){ //if the loantransfer is bigger than the loanAmount
            loanAmount = 0 //set loanAmount to zero
            bankBalance += loanTransfer - loanAmount //add the extra money to the bankBalance
            earned = 0; //set earned to zero
        }else{
            loanAmount -= loanTransfer //withdrawing the down-payment from the loan
            bankBalance += earned - loanTransfer //Adding the earned money minus the ten percent to the bankBalance
            earned = 0; //resetting earned
        }
        if (loanAmount === 0){ //if the new loanAmount is zero one has downpayed the loan
            elLoanField.innerText='' //update the loanfield shown to user to be empty
            elLoanBalance.innerText='' //update the loanamount shown to user to be empty
            elPayLoanButton.style.display="none" //hide/stop displaying the pay loan button
            if (computerBought){ //if the user has bought a computer
                elLoanButton.disabled=false //enable the get loan button again
            }
        }else{ //if the loan is not down-payed
            elLoanBalance.innerText= loanAmount + ' kr.' //update the loanbalance shown to user
        }
    }else{ //if one does'nt have a loan
        bankBalance += earned //update the bank balance value
        earned = 0 //reset earned value
    }
    elBankBalance.innerText = bankBalance + ' kr.' //update the bank balance shown to user
    elWorkBalance.innerText = earned + ' kr.' //update the earned money shown to user
})


//button-listener if one wants to earn money by working
elWorkButton.addEventListener('click', function () {
    earned += 100 //increase earned value by 100
    elWorkBalance.innerText= earned + ' kr.' //update the earned money shown to user
})


//button-listener if one wants to use all the earned money to pay down a loan
elPayLoanButton.addEventListener('click',  function(){
    if (earned > loanAmount){ //if the earned money is more than the loan amount
        earned -= loanAmount //update the earned value to be decreased with the loan amount
        loanAmount = 0 //set loanAmount to zero
        elLoanField.innerText='' //update the loanfield shown to user to be empty
        elLoanBalance.innerText='' //update the loanamount shown to user to be empty
        elPayLoanButton.style.display="none" //hide/stop displaying the pay loan button
        if (computerBought){ //if the user has bought a computer
            elLoanButton.disabled=false //enable the get loan button again
        }
    } else{ //if the earned money is equal or less to the loanAmount
        loanAmount -= earned //set loanAmount to be decreased with earned value
        earned = 0 //reset earned value to zero
        if (loanAmount === 0){ //if the loan is downpayed I do the same as above to hide the loan-functionality and elements
            elLoanField.innerText=''
            elLoanBalance.innerText=''
            elPayLoanButton.style.display="none"
            if (computerBought){
                elLoanButton.disabled=false
            }
        }else{ // if the loan is not downpayed
            elLoanBalance.innerText = loanAmount + ' kr.' //update the loanamount shown to user
        }
    }
    elWorkBalance.innerText = earned + ' kr.' //update the earned money shown to user
})


//selectbar-listener to see which computer is chosen, and update user information
elComputersSelectBar.addEventListener('change', function () {
    const value = Number(this.value) //get the value of the select bar
    if (value === -1){ //if the value is
        computerPrice = 0 //update the computerPrice to zero
        elInfoSection.style.display="none" //hide /stop displaying the infosection
        elComputerImage.style.display="none" //and the picture
        elFeatureHeader.style.display="none" //and the feature
        elComputerFeature.innerText = '' //and update feauturetext to be empty
        return; //exit the eventlistener function
    }
    //get the selected computer
    const selectedComputer = COMPUTERS.find( function (item) {
        return item.id === value
    })
    //get the features of the computer and make them into string (since i have stored them in an array)
    let featureArray = Array.from(selectedComputer.feature)
    let featureText = ''
    for (let i = 0; i < featureArray.length; i++){
        featureText+= featureArray[i]  + "\n"
    }
    elReturnText.style.display="none" //hide /stop displaying the returnText
    elInfoSection.style.display="initial" //start showing the infoscreen
    elFeatureHeader.style.display="initial" //startShowing the feature header
    elComputerFeature.innerText = featureText //update the featuretext shown to user
    elComputerImage.src = selectedComputer.image //update the image shown to user
    elComputerImage.style.display="initial" //start showing the image
    elComputerName.innerText = selectedComputer.name //update the computername shown to user
    elSummaryInfo.innerText = selectedComputer.description //update the description shown to user
    elPrice.innerText = selectedComputer.price + ' kr.' //update the computer price shown to user
    computerPrice = Number(selectedComputer.price) //update the value of computerPrice variable
})


//button-listener if one wants to buy a computer
elBuyComputerButton.addEventListener('click', function (){
    if (computerPrice > bankBalance + loanAmount){ //check if one has enough money to buy the computer
        alert("You don't have enough money in the bank to buy this computer")  //inform user that they don't have enough money
        return;//exit the eventlistener function
    }else{ //if one has enough money
        bankBalance -= computerPrice; //deduct the computerprice from the bankBalance
        elBankBalance.innerText = bankBalance + ' kr.' //update the bank balance shown to user
        computerBought = true //update computerought variable so that user can get a new loan
        if (loanAmount === 0){ //if the loanAmount is zero
            elLoanButton.disabled=false //enable the get loan button
        }
        elInfoSection.style.display="none" //hide/ stop displaying the infosection
        elComputerImage.style.display="none" //and image
        elFeatureHeader.style.display="none" //and featureheader
        elComputerFeature.innerText = '' //and update feature text to be empty

        const chosenComputerId = Number(elComputersSelectBar.value) //get selected computerId
        //get the selected computer
        const chosenCompuer = COMPUTERS.find( function (item) {
            return item.id === chosenComputerId
        })
        let returnString = "You are now the proud owner of a " + chosenCompuer.name
        elReturnText.innerText = returnString //set returntext to inform user tey have bought a computer
        elReturnText.style.display="initial" //start displaying returnText to user
    }
})






# Assignment 4

This project is a web application for a computer store. 

It simulates three main elements; 
1. A bank where you have a balance and can have a loan. 
2. That you are working and earning money you can transfer 
   to your bank account or that you can pay back your loan with.
3. A computer store where you can buy a computer with the money from 
   your bank.
   

To run the project on your localhost use your terminal to go to the root 
directory of this assignment called assignment4 and run 
```
http-server
```

